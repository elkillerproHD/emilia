import {spellsManifest} from "./spells";

const express = require('express')
const path = require('path');
const multer = require('multer');
const app = express()

const bodyParser = require('body-parser');

// for parsing application/json
app.use(bodyParser.json());

app.use(express.static('images'));

const port = 3000
const emilia = require('../lib')

const MongoClient = require('mongodb').MongoClient;

const dbUri = "mongodb://localhost:27017/emilia"
const client = MongoClient(dbUri);

const imageStorage = multer.diskStorage({
	destination: 'images',
	filename: (req, file, cb) => {
		const extension = file.originalname.split('.').pop();
		cb(null, req.body.name+'.'+extension)
	}
});

const imageUpload = multer({
	storage: imageStorage,
	limits: {
		fileSize: 1000000 // 1000000 Bytes = 1 MB
	},
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(png|jpg|svg)$/)) {
			return cb(new Error('Please upload a Image'))
		}
		cb(undefined, true)
	}
})

app.post('/emilia', (req, res) => {
	const query = req.body
	const waifu = emilia.getWaifu()
	waifu.handleSpell(query, req, res)
})

app.post('/uploadImage', imageUpload.single('image'), (req, res) => {
	res.send({...req.body, ...req.file})
}, (error, req, res, next) => {
	res.status(400).send({ error: error.message })
})

app.listen(port, async () => {
	try {
		await client.connect();
		const db = client.db('emilia')
		emilia.initialize({
			db,
			spells: spellsManifest,
			hash: '1234567812345678123456781234567812345678123456781234567812345678'
		})
	} catch (e) {
		throw e
	}
	console.log(`Example app listening at http://localhost:${port}`)
})

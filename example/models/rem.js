import WaifuModel from "../../src/utils/baseModel";

export const collection = "WAIFU"

export const model = [
	{
		name: "seiyuu",
		type: "string",
		required: false,
	},
	{
		name: "kawaii",
		type: "boolean",
		required: false,
	},
	{
		name: 'image',
		type: 'img'
	}
]

export class Waifu extends WaifuModel {
	constructor(data) {
		super({
			collection: collection,
			data,
			model
		});
	}
}

import { ObjectID } from 'mongodb'
import {GenerateString, IsValidJSONString, validateEmail} from "./utils/functions";
import {errors_message} from "./utils/errors";
import {HashPassword} from "./utils/encryptation";
import {getEmiliaConfig} from "./index";

export async function authMiddleware (query)  {
  return new Promise(async (resolve, reject) => {
    if(!IsValidJSONString(query)){
      reject ({
        errors: true,
        err_message: errors_message.invalid_json_query
      })
    }
    const jsonDta = JSON.parse(query);
    if(jsonDta.type === "create-user-pass") {
      try {
        const result = await this.createNewUserWithPass(jsonDta);
        resolve(result);
      } catch (e) {
        reject(e);
      }
    } else if(jsonDta.type === "sing-in-user_email-password"){
      try {
        const result = await this.singInWithEmailPassword(jsonDta);
        resolve(result);
      } catch (e) {
        reject(e);
      }
    } else if(jsonDta.type === "send-recover-password"){
      try {
        const result = await this.sendResetPassword(jsonDta);
        resolve(result);
      } catch (e) {
        reject(e);
      }
    }
  })
}
export function CreateUser ({name, email, password, user, key})         {
  const check = [name, email, password, user];
  let isValid = true;
  check.forEach(dta => {
    if(typeof dta !== "string"){
      isValid = false;
      return
    }
    if(dta.length < 1){
      isValid = false;
    }
  })
  const salt = GenerateString(32);
  const hashPassword = HashPassword(password, key, salt)
  if(isValid){
    return {
      name,
      email,
      password: hashPassword,
      user,
      salt,
      authenticated: false,
      attacked: false,
      locked_account_end_date: '',
      wrongIntentsSingIn: 0
    };
  } else {
    return false;
  }
}
export async function createNewUserWithPass (query) {
  return new Promise(async (resolve, reject) => {
    const {db, secretKeyHash} = getEmiliaConfig();
    try {
      const checkUserExistQuery = {
        "$or": [{email: query.data.email}, {user: query.data.user}]
      }
      const checkUserExist = await db.collection("users").find(checkUserExistQuery);

      const json = [];
      await checkUserExist.forEach((obj) => json.push(obj));
      if(json.length > 0){
        if(json[0].user === query.data.user){
          reject({
            errors:true,
            err_code: "username_is_taken",
            err_message: errors_message.username_is_taken
          })
          return;
        } else {
          reject({
            errors:true,
            err_code: "email_is_taken",
            err_message: errors_message.email_is_taken
          })
          return;
        }
      }
      const user = CreateUser({...query.data, key: secretKeyHash});
      if(!user){
        reject({
          errors:true,
          err_message: errors_message.error_user_parameters_not_valid
        })
      }
      try {
        const result = await db.collection("users").insertOne(user);
        if(result.insertedCount >= 1){
          try {
            await this.sendAuthenticationWithId(result.ops[0]._id.toString())
            resolve(result.ops[0])
          } catch (e) {
            reject(e)
          }
        } else {
          reject({
            errors:true,
            err_message: errors_message.error_when_creating_object
          })
        }
      } catch (e) {
        reject({
          errors:true,
          err_message: errors_message.error_when_creating_object
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.not_user_found_for_user_email
      })
    }
  })
}
export async function sendAuthenticationWithId (userId, resend = false) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    if(userId === null || userId === undefined){
      reject({
        errors:true,
        err_message: errors_message.invalid_resend_authentication_account_code_with_userId
      })
      return;
    }
    if(typeof userId !== "string"){
      reject({
        errors:true,
        err_message: errors_message.invalid_resend_authentication_account_code_with_userId
      })
      return;
    }
    if(userId.length < 1){
      reject({
        errors:true,
        err_message: errors_message.invalid_resend_authentication_account_code_with_userId
      })
      return;
    }
    let dateToSave = new Date(Date.now());
    dateToSave.setDate(dateToSave.getDate() + 2);
    const authUserCodeQuery = {
      user_id: userId,
      date: dateToSave.toISOString()
    }
    try {
      const authUserCode = await db.collection("auth_user_code").insertOne(authUserCodeQuery);
      if(authUserCode.insertedCount >= 1) {
        if(!resend){
          console.dir(`Para autenticar este usuario ingresar a http://localhost:3000/auth-account?q=${authUserCode.ops[0]._id}  `)
        } else {
          console.dir(`El enlace ha expirado para autenticar este usuario ingresa a http://localhost:3000/auth-account?q=${authUserCode.ops[0]._id}  `)
        }
        resolve()
      } else {
        reject({
          errors:true,
          err_message: errors_message.error_when_creating_object
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_creating_object
      })
    }
  })
}
export async function authenticateAccount (idAuthCode) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    if(idAuthCode === null || idAuthCode === undefined){
      reject({
        errors:true,
        err_message: errors_message.invalid_authentication_account_code
      })
      return;
    }
    if(typeof idAuthCode !== "string"){
      reject({
        errors:true,
        err_message: errors_message.invalid_authentication_account_code
      })
      return;
    }
    if(idAuthCode.length < 1){
      reject({
        errors:true,
        err_message: errors_message.invalid_authentication_account_code
      })
      return;
    }
    const queryAuthenticateAccount = {
      _id: new ObjectID(idAuthCode)
    }
    try {
      const result = await db.collection("auth_user_code").findOne(queryAuthenticateAccount);
      const checkDate = new Date(result.date);
      const now = new Date(Date.now())
      if(now.getTime() > checkDate.getTime()){
        await this.sendAuthenticationWithId(result.user_id.toString(), true)
        reject({
          errors:true,
          err_message: errors_message.expired_authentication_account_code
        })
        return
      }

      // UPDATE: AUTHENTICATE ACCOUNT
      const filter = { _id: new ObjectID(result.user_id) };
      const updateDocument = {
        $set: {
          authenticated: true,
        },
      };
      try {
        const authAccount = await db.collection("users").updateOne(filter, updateDocument);
        if(authAccount.result.ok >= 1){
          resolve()
        } else {
          reject({
            errors:true,
            err_message: errors_message.error_when_authenticating_account_code
          })
        }
        resolve(result)
      } catch (e) {
        reject({
          errors:true,
          err_message: errors_message.error_when_authenticating_account_code
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_authenticating_account_code
      })
    }
  })
}
export async function intentSingInWrongPassword (user) {
  return new Promise(async (resolve, reject) => {
    try {
      const {db} = getEmiliaConfig();
      let newLockedDate, attacked;
      if(user.wrongIntentsSingIn < 3) {
        newLockedDate = new Date(Date.now())
        attacked = false;
      } else if(user.wrongIntentsSingIn === 3){
        newLockedDate = new Date()
        newLockedDate.setMinutes(newLockedDate.getMinutes() + 5)
        attacked = true;
      } else if(user.wrongIntentsSingIn > 3 && user.wrongIntentsSingIn <= 6){
        newLockedDate = new Date()
        newLockedDate.setMinutes(newLockedDate.getMinutes() + 10)
        attacked = true;
      } else if(user.wrongIntentsSingIn > 6 && user.wrongIntentsSingIn <= 9){
        newLockedDate = new Date()
        newLockedDate.setMinutes(newLockedDate.getMinutes() + 30)
        attacked = true;
      } else if(user.wrongIntentsSingIn > 9 && user.wrongIntentsSingIn <= 12){
        newLockedDate = new Date()
        newLockedDate.setMinutes(newLockedDate.getMinutes() + 60)
        attacked = true;
      } else if(user.wrongIntentsSingIn > 12 && user.wrongIntentsSingIn <= 15){
        newLockedDate = new Date()
        newLockedDate.setMinutes(newLockedDate.getMinutes() + 60 * 24)
        attacked = true;
      } else if(user.wrongIntentsSingIn > 15 && user.wrongIntentsSingIn){
        newLockedDate = new Date()
        newLockedDate.setMinutes(newLockedDate.getMinutes() + 60 * 24 *5)
        attacked = true;
      }
      const filter = { _id: new ObjectID(user._id) };

      const updateUserQuery = {
        $set: {
          locked_account_end_date: newLockedDate.toISOString(),
          wrongIntentsSingIn: user.wrongIntentsSingIn + 1,
          attacked: attacked
        },
      }

      try {
        const CreateIntentWrongPass = await db.collection("users").updateOne(filter, updateUserQuery);
        if(CreateIntentWrongPass.result.ok < 1) {
          return reject({
            errors:true,
          })
        }
        resolve()
      } catch (e) {
        return reject({
          errors: true
        })
      }
    } catch (e) {
      return reject({
        errors: true
      })
    }
  })
}
export async function singInWithEmailPassword (query) {
  return new Promise(async (resolve, reject) => {
    const {db, secretKeyHash} = getEmiliaConfig();
    const emailOrUser = query.data["email_user"];
    const pass = query.data["password"];
    let queryFindUser;
    if(validateEmail(emailOrUser)){
      queryFindUser = {
        email: emailOrUser
      }
    } else {
      queryFindUser = {
        user: emailOrUser
      }
    }
    try{
      const user = await db.collection("users").findOne(queryFindUser);

      if(user === null){
        reject({
          errors:true,
          err_message: errors_message.not_user_found_for_user_email,
          err_code: "not_user_found_for_user_email"
        })
      } else {
        if(user.attacked){
          const checkDate = new Date(user["locked_account_end_date"]);
          const now = new Date(Date.now())
          if(now.getTime() < checkDate.getTime()){
            reject({
              errors:true,
              err_message: errors_message.banned_account + checkDate.toISOString()
            })
            return
          }
        }

        const hashPassword = HashPassword(pass, secretKeyHash, user.salt)
        if(hashPassword !== user.password){
          await this.intentSingInWrongPassword(user)
          reject({
            errors:true,
            err_message: errors_message.incorrect_password_authentication,
            err_code: "incorrect_password_authentication"
          })
          return;
        }
        try {
          const filter = { _id: new ObjectID(user._id) };
          const updateDocument = {
            $set: {
              attacked: false,
              locked_account_end_date: '',
              wrongIntentsSingIn: 0
            },
          };
          const removeAttackedUser = await db.collection("users").updateOne(filter, updateDocument);
          if(removeAttackedUser.result.ok < 1) {
            reject({
              errors:true,
              err_message: errors_message.error_at_unban_account
            })
          }
        } catch (e) {
          reject({
            errors:true,
            err_message: errors_message.error_at_unban_account
          })
        }
        try {
          const queryFindSesion = {
            user_id: user._id
          }
          const deleteSesionQuery = await db.collection("sesions").deleteOne(queryFindSesion);
          if(deleteSesionQuery.result.ok >= 1){
            try{
              let dateToSave = new Date(Date.now());
              dateToSave.setDate(dateToSave.getDate() + 15);
              const queryCreateSesion = {
                user_id: user._id,
                date: dateToSave.toISOString()
              }
              const createSesionQuery = await db.collection("sesions").insertOne(queryCreateSesion);
              if(createSesionQuery.insertedCount >= 1){
                resolve({
                  sesion: createSesionQuery.ops[0]._id,
                  authenticated: user.authenticated
                })
              } else {
                reject({
                  errors:true,
                  err_message: errors_message.error_when_creating_sesion_for_create_new_sesion
                })
              }
            } catch (e) {
              reject({
                errors:true,
                err_message: errors_message.error_when_creating_sesion_for_create_new_sesion
              })
            }

          } else {
            reject({
              errors:true,
              err_message: errors_message.error_when_deleting_sesion_for_create_new_sesion
            })
          }
        } catch (e) {
          reject({
            errors:true,
            err_message: errors_message.error_when_deleting_sesion_for_create_new_sesion
          })
        }
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.server_error_when_finding_user,
        err_code: "not_user_found_for_user_email"
      })
    }
  })
}

import {
  createQuery,
  deleteDocumentQuery, findByIdQuery, findByValuesQuery,
  getAllCollectionQuery,
  handleSpell, realDeleteDocumentQuery,
  setDocumentQuery,
  updateDocumentQuery
} from "./middleware";
import {
  authenticateAccount,
  authMiddleware,
  createNewUserWithPass, intentSingInWrongPassword,
  sendAuthenticationWithId,
  singInWithEmailPassword
} from "./authentication";
import {checkRecoverPassword, createRecoverPassword, sendResetPassword} from "./resetPassword";

class Emilia {
  constructor(props) {

    // declare DB manager middleware
    this.handleSpell = handleSpell;
    this.createQuery = createQuery;
    this.getAllCollectionQuery = getAllCollectionQuery;
    this.setDocumentQuery = setDocumentQuery;
    this.updateDocumentQuery = updateDocumentQuery;
    this.deleteDocumentQuery = deleteDocumentQuery;
    this.realDeleteDocumentQuery = realDeleteDocumentQuery;
    this.findByIdQuery = findByIdQuery;
    this.findByValuesQuery = findByValuesQuery;

    // authentication middleware
    this.authMiddleware = authMiddleware;
    this.createNewUserWithPass = createNewUserWithPass;
    this.authenticateAccount = authenticateAccount;
    this.sendAuthenticationWithId = sendAuthenticationWithId;
    this.singInWithEmailPassword = singInWithEmailPassword;
    this.intentSingInWrongPassword = intentSingInWrongPassword;
    this.sendResetPassword = sendResetPassword;

    this.checkRecoverPassword = checkRecoverPassword;
  }
}
const initialize = (props) => {
  global.emiliaConfig = {
    db: props.db,
    secretKeyHash: props.secretKeyHash,
    spells: props.spells
  }
  global.Emilia = new Emilia()
}

const getEmiliaConfig = () => {
  return global.emiliaConfig
}

const getWaifu = () => {
	if(global.Emilia === undefined) global.Emilia = new Emilia()
  return global.Emilia
}

export { initialize, getEmiliaConfig, getWaifu }

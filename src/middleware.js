import { ObjectID } from 'mongodb';
import {errors_message} from "./utils/errors";
import {getEmiliaConfig} from "./index";

export async function handleSpell (query, req, res) {
  const { spells } = getEmiliaConfig()
  return new Promise(async (resolve, reject) => {
    const jsonDta = query;
    if(spells[jsonDta.spell] !== undefined){
      const spellToCast = spells[jsonDta.spell]
      try {
        await spellToCast({data: jsonDta.data, req, res})
      } catch (e) {
        return reject(e)
      }
      resolve()
    }
  })
}
export async function createQuery (query) {
  return new Promise(async (resolve, reject) => {
    const { db } = getEmiliaConfig();
    try {
      delete query.data.id
      const result = await db.collection(query.collection).insertOne(query.data);
      if(result.insertedCount >= 1){
        const id = String(result.ops[0]._id)
        delete result.ops[0]._id
        resolve({
          id,
          ...result.ops[0]
        })
      } else {
        reject({
          errors:true,
          err_message: errors_message.error_when_creating_object
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_creating_object
      })
    }
  })
}
export async function getAllCollectionQuery (query) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    try{
      const result = await db.collection(query.collection).find();
      const json = [];
      await result.forEach((obj) => {
        const id = String(obj._id)
        delete obj._id
        const noIdObj = {
          id,
          ...obj
        }
        json.push(noIdObj)
      });
      resolve(json)
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_querying_all_the_collection
      })
    }
  })
}
export async function setDocumentQuery (query) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    const findByIdQuery = {
      _id: new ObjectID(query.data.id)
    }
    delete query.data.id
    const options = { upsert: true };
    const update = {
      $set: query.data
    }
    try{
      const result = await db.collection(query.collection).updateOne(findByIdQuery, update, options);
      if(result.result.ok >= 1){
        resolve()
      } else {
        reject({
          errors:true,
          err_message: errors_message.error_when_creating_object
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_creating_object
      })
    }
  })
}
export async function updateDocumentQuery (query) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    const qr = {
      _id: new ObjectID(query.data.id)
    }
    const options = { upsert: false };
    delete query.data.id
    const update = {
      $set: query.data
    }
    try{
      const result = await db.collection(query.collection).updateOne(qr, update, options);
      if(result.result.ok >= 1){
        resolve()
      } else {
        reject({
          errors:true,
          err_message: errors_message.error_when_querying_update
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_querying_update
      })
    }
  })
}
export async function realDeleteDocumentQuery (query) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    const qr = {
      _id: new ObjectID(query.data.id)
    }
    try {
      const result = await db.collection(query.collection).deleteOne(qr);
      if(result.result.ok >= 1){
        resolve()
      } else {
        reject({
          errors:true,
          err_message: errors_message.error_when_querying_delete
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_querying_delete
      })
    }
  })
}export async function deleteDocumentQuery (query) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    const qr = {
      _id: new ObjectID(query.data.id)
    }
    try {
      const findToDelete = await db.collection(query.collection).findOne(qr);
      try {
        const insertDeleted = await db.collection(`${query.collection}_deleted`).insertOne(findToDelete);
        try {
          const result = await db.collection(query.collection).deleteOne(qr);
          if(result.result.ok >= 1){
            resolve()
          } else {
            reject({
              errors:true,
              err_message: errors_message.error_when_querying_delete
            })
          }
        } catch (e) {
          reject({
            errors:true,
            err_message: errors_message.error_when_querying_delete
          })
        }
      } catch (e) {
        reject({
          errors:true,
          err_message: errors_message.error_when_creating_object
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_querying_delete
      })
    }
  })
}

export async function findByIdQuery (query) {
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    const qr = {
      _id: new ObjectID(query.data.id)
    }
    try{
      const result = await db.collection(query.collection).findOne(qr);
      const id = String(result._id)
      delete result._id
      const noIdObj = {
        id,
        ...result
      }
      resolve(noIdObj)
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_findById
      })
    }
  })
}
function GetComparisonQueryOperator (symbol)  {
  if(
    symbol !== "=="
    && symbol !== "!="
    && symbol !== ">="
    && symbol !== "<="
    && symbol !== "<"
    && symbol !== ">"
  ) {
    return false
  }
  if(symbol === "=="){
    return "$eq";
  } else if(symbol === "!="){
    return "$ne";
  } else if(symbol === ">="){
    return "$gte";
  } else if(symbol === "<="){
    return "$lte";
  } else if(symbol === "<"){
    return "$lt";
  } else if(symbol === ">"){
    return "$gt";
  } else if(symbol === "array-contains"){
  	return "$all";
	}
}
export async function findByValuesQuery (query){
  return new Promise(async (resolve, reject) => {
    const {db} = getEmiliaConfig();
    let qr = {};
    let sort = {};
    query.data.forEach(dta => {
      let comparisonQueryOperator = GetComparisonQueryOperator(dta.condition);
      if(!comparisonQueryOperator){
        reject({
          errors:true,
          err_message: errors_message.invalid_symbol_comparison
        })
      } else {
        qr[dta.key] = {[comparisonQueryOperator]: dta.value}
      }
    })
    let useSort = true;
    let useLimit = true;
    if(query.sort !== undefined && query.sort !== null){
      if(typeof query.sort !== 'object'){
        reject({
          errors:true,
          err_message: errors_message.error_when_findByValues_invalid_sort
        })
        return;
      }
      else {
        if(typeof query.sort.key !== 'string' || typeof query.sort.value !== 'string'){
          reject({
            errors:true,
            err_message: errors_message.error_when_findByValues_invalid_sort
          })
          return;
        } else {
          if(query.sort.key.length < 1 || typeof query.sort.value < 1){
            reject({
              errors:true,
              err_message: errors_message.error_when_findByValues_invalid_sort
            })
            return;
          } else {
            sort = {[query.sort.key]: query.sort.value === "desc" ? -1 : 1}
          }
        }
      }
    } else {
      useSort = false;
    }
    if(query.limit === undefined || query.limit === null){
      useLimit = false;
    } else {
      if(typeof query.limit !== 'number'){
        reject({
          errors:true,
          err_message: errors_message.error_when_findByValues_invalid_limit
        })
        return;
      }
    }
    try{
      let result;
      if(useSort && useLimit){
        result = await db.collection(query.collection).find(qr).sort(sort).limit(query.limit);
      } else if(useSort && !useLimit){
        result = await db.collection(query.collection).find(qr).sort(sort);
      } else if(!useSort && useLimit){
        result = await db.collection(query.collection).find(qr).limit(query.limit);
      } else if(!useSort && !useLimit){
        result = await db.collection(query.collection).find(qr);
      }
      const json = [];
      await result.forEach((obj) => {
        const id = String(obj._id)
        delete obj._id
        const noIdObj = {
          id,
          ...obj
        }
        json.push(noIdObj)
      });
      resolve(json)
    } catch (e) {
      console.log(e)
      reject({
        errors:true,
        err_message: errors_message.error_when_findByValues
      })
    }
  })
}

// // // /// /// /// // QUERYS EXAMPLES

// CREATE QUERY
// const query = {
//   type: "create",
//   collection: "USER_COLLECTION_TEST",
//   data: {
//     name: "Federico",
//     surname: "Fernandez",
//   },
//   auth: {
//     id: "ramdom_fake_id_for_this_test"
//   }
// }

// QUERY ALL COLLECTION
// const query = {
//   type: "all",
//   collection: "USER_COLLECTION_TEST",
//   auth: {
//     id: "ramdom_fake_id_for_this_test"
//   }
// }

// SET QUERY
// const query = {
//   type: "set",
//   collection: "USER_COLLECTION_TEST",
//   data: {
//     _id: "test_user_id",
//     name: "Federico",
//     surname: "Fernandez",
//   },
//   auth: {
//     id: "ramdom_fake_id_for_this_test"
//   }
// }

// UPDATE QUERY
// const query = {
//   type: "update",
//   collection: "USER_COLLECTION_TEST",
//   data: {
//     _id: "test_user_id",
//     name: "Federico",
//     surname: "Fernandez",
//   },
//   auth: {
//     id: "ramdom_fake_id_for_this_test"
//   }
// }

// DELETE QUERY
// const query = {
//   type: "delete",
//   collection: "USER_COLLECTION_TEST",
//   data: {
//     _id: "test_user_id",
//   },
//   auth: {
//     id: "ramdom_fake_id_for_this_test"
//   }
// }

// FINDBYID QUERY
// const query = {
//   type: "findById",
//   collection: "USER_COLLECTION_TEST",
//   data: {
//     _id: "test_user_id",
//   },
//   auth: {
//     id: "ramdom_fake_id_for_this_test"
//   }
// }

// FINDBYVALUES QUERY
// const query = {
//   type: "findByValues",
//   collection: "USER_COLLECTION_TEST",
//   data: [
//     {
//       key: "name",
//       type: "==",
//       value: "Federico"
//     }
//   ],
//   sort: {
//     key: "name",
//     value: "desc"
//   },
//   limit: "10",
//   auth: {
//     id: "ramdom_fake_id_for_this_test"
//   }
// }

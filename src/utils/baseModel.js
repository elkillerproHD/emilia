import {validateModel} from './validateModel';
import { getWaifu } from '../index'
import {typedef} from 'bycontract';
import {handleFiles} from "./files";

const customTypes = [
  {
    name: 'img',
    type: {
			filename: 'string',
			isFile: 'bool'
    },
  },
];

for(const customType of customTypes){
	typedef(customType.name, customType.type)
}

/*
 *  JSON de ejemplo para realizar multiple writes
 * [{
 * // CHECK es el modelo a utilizar en la vadilacion con bycontract
 *   check: foodModel
 *   model: "FOOD-CATEGORY",
 *   action: "add",
 * // DATA tiene que ser el modelo normal con los datos
 *   data: [
 *     name: "Pastas",
 *     img: {
 *       name: "pastas.png",
 *       path: "asd/pastas.png",
 *       fileType: "image/png"
 *     }
 *   ]
 * }]
 * */

const errorCondition =
  'The Condition is not valid. The valid contions are == != < <= > >= and your condition is ';
const errorInSearchParameters =
  'You need to pass valid parameters ({type, value})';
const errorID = 'You must provide an ID for search';
const errorIDMany2Many =
  'You must provide an ID in the constructor to7 work with many2many';
const errorMany2ManyFields =
  'You must provide collection, name and table name in the model of Many2Many';

const preModels = {
  name: 'id',
  type: 'string',
  required: true,
};

export default class WaifuModel {
  constructor(props) {
    this.emilia = getWaifu();
    this.data = props.data;
    this.collection = props.collection;
    this.model = props.model;
  }

  create = async () => {
    const {emilia, model, collection} = this;
    let {data} = this;

    validateModel(model, data);
    data = await handleFiles(data, emilia);

		return new Promise(async (resolve, reject) => {
			try {
				this.data = await emilia.createQuery({ collection, data })
				resolve(this.data)
			} catch (e) {
				reject(e)
			}
		})
  };
  all = async () => {
    const { emilia, collection } = this;
		return emilia.getAllCollectionQuery({ collection })
  };
  set = async () => {
    const {emilia, model, collection} = this;
    let { data } = this

    validateModel([preModels, ...model], data);
    data = await handleFiles(data);

		return new Promise(async (resolve, reject) => {
			try {
				// NOT REMOVE destructuring to avoid remove object id reference
				await emilia.setDocumentQuery({ collection, data: {...data} })
				resolve(this.data)
			} catch (e) {
				reject(e)
			}
		})
  };
  update = async () => {
    const { emilia, model, collection } = this;
    let { data } = this

    validateModel([preModels, ...model], data);
		data = await handleFiles(data);

		return new Promise(async (resolve, reject) => {
			try {
				// NOT REMOVE destructuring to avoid remove object id reference
				await emilia.updateDocumentQuery({ collection, data: {...data}})
				resolve(data)
			} catch (e) {
				reject(e)
			}
		})
  };
  realRemove = async () => {
    const {collection, emilia, data} = this;
    validateModel([preModels], {id: data.id});

    return emilia.realDeleteDocumentQuery({collection, data: { id: data.id }})
  }
  delete = async () => {
		const {collection, emilia, data} = this;
		validateModel([preModels], {id: data.id});

		return emilia.deleteDocumentQuery ({ data: {id: data.id}, collection })
	}
  findById = async (data) => {
    const { id = false } = data;
    const { emilia, collection } = this;
    if (!id) {
      console.error(errorID);
      return Promise.reject({error: errorID});
    }

		return new Promise(async (resolve, reject) => {
			try {
				this.values = await emilia.findByIdQuery({ collection, data: { id } })
				resolve(this.values)
			} catch (e) {
				reject(e)
			}
		})
  };
  findByValues = async (dta) => {

    const {
      conditions,
      orderBy = false,
      limit = false,
    } = dta;
    // CONDITION EXAMPLE
    // [{key: "name", condition: "==", value: "Federico"}]
    if (
      conditions.length < 1
    ) {
      console.error(errorInSearchParameters);
      return Promise.reject({
        err_message: errorInSearchParameters
      });
    }

    const {emilia, collection} = this;
    let query = {
      data: []
    };

    conditions.map((c) => {
      const { key, value, condition } = c;
      if (!key || !value) {
        console.error(errorInSearchParameters);
        return Promise.reject({
          err_message: errorInSearchParameters
        });
      }
      if (
        condition !== false &&
        condition !== '==' &&
        condition !== '<' &&
        condition !== '>' &&
        condition !== '>=' &&
        condition !== '<=' &&
        condition !== 'array-contains'
      ) {
        console.error(errorCondition, condition);
        return Promise.reject({
          err_message: `${errorCondition}${condition}`
        });
      }
      query.data.push(c);
    })

    if (orderBy) {
      query.sort(orderBy);
    }
    if (limit) {
      query.limit(limit);
    }

		return new Promise(async (resolve, reject) => {
			try {
				this.values = await emilia.findByValuesQuery({collection, ...query})
				resolve(this.values)
			} catch (e) {
				reject(e)
			}
		})
  };
}

// export function MultipleWriteJson(props) {
//   const {data, isFront} = props;
//   const db = db.waifu();
//
//   // Primero agrupamos las mismas colecciones
//   const sameCollections = {};
//   data.forEach((dta) => {
//     if (sameCollections[dta.model]) {
//       sameCollections[dta.model].push(dta);
//     } else {
//       sameCollections[dta.model] = [];
//       sameCollections[dta.model].push(dta);
//     }
//   });
//   Object.values(sameCollections).forEach((dta, index) => {
//     const toMap = [];
//     let objects = [];
//     const actualKey = Object.keys(sameCollections)[index];
//     dta.map((query) => {
//       if (objects.length < 11) {
//         objects.push(query);
//       } else {
//         toMap.push(objects);
//         objects = [];
//       }
//     });
//     if (objects.length > 0) {
//       toMap.push(objects);
//     }
//     toMap.forEach(async (dta) => {
//       for (const query of dta) {
//         switch (query.action) {
//           case 'add':
//             validateModel(query.check, query.data);
//             try {
//               await db.createQuery({collection: query.model, data: query.data});
//             } catch (e) {
//               console.error(e)
//             }
//             break;
//           case 'set':
//             query.check.push(preModels);
//             validateModel(query.check, query.data);
//             try {
//               await db.setDocumentQuery({collection: query.model, data: query.data});
//             } catch (e) {
//               console.error(e)
//             }
//             break;
//           case 'delete':
//             validateModel(query.check, query.data);
//             try {
//               await db.deleteDocumentQuery({collection: query.model, data: query.data});
//             } catch (e) {
//               console.error(e)
//             }
//             break;
//           case 'remove':
//             validateModel(query.check, query.data);
//             try {
//               await db.realDeleteDocumentQuery({collection: query.model, data: query.data});
//             } catch (e) {
//               console.error(e)
//             }
//             break;
//           case 'update':
//             query.check.push(preModels);
//             validateModel(query.check, query.data);
//             try {
//               await db.updateDocumentQuery({collection: query.model, data: query.data});
//             } catch (e) {
//               console.error(e)
//             }
//             break;
//           default:
//         }
//         console.log(`Model: ${query.model} Seeded...`)
//       }
//     });
//   });
// }
